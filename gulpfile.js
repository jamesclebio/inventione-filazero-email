var gulp = require('gulp'),
    compass = require('gulp-compass'),
    connect = require('gulp-connect'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    inlineCss = require('gulp-inline-css'),
    plumber = require('gulp-plumber'),
    replace = require('gulp-replace');

// Default
gulp.task('default', function () {
  console.log('\nPROJECT COMMAND LIST\n');
  console.log('gulp dev');
  console.log('gulp images');
  console.log('gulp styles');
  console.log('gulp build');
  console.log('\n--\n');
});

// Dev
gulp.task('dev', ['connect', 'watch:dev']);

// Prod
gulp.task('prod', ['watch:prod']);

// Connect
gulp.task('connect', function () {
  connect.server({
    port: 8000,
    livereload: true
  });
});

// Watch dev
gulp.task('watch:dev', function () {
  gulp.watch('sass/**/*', ['styles']);
  gulp.watch('html/**/*', ['templates']);
});

// Watch prod
gulp.task('watch:prod', function () {
  gulp.watch('html/**/*', ['build']);
});

// Images
gulp.task('images', function () {
  gulp.src('img/**/*')
    .pipe(plumber())
    .pipe(imagemin({
      optimizationLevel: 5
    }))
    .pipe(gulp.dest('img'))
    .pipe(connect.reload());
});

// Styles
gulp.task('styles', function () {
  gulp.src('sass/**/*')
    .pipe(plumber())
    .pipe(compass({
      style: 'expanded',
      sass: 'sass',
      css: 'css',
      image: 'img'
    }))
    .pipe(connect.reload());
});

// Templates
gulp.task('templates', function () {
  gulp.src('html/**/*.html')
    .pipe(connect.reload());
});

// Build
gulp.task('build', function () {
  var buildPath = '../../Core/Common/Common/Infraestructure/EmailTemplate',
      imgHost = '{{site-root}}/images/email/',
      delOptions = {
        force: true
      };

  del(buildPath, delOptions, function () {
    gulp.src([
        'html/**/*.html',
        '!html/_*.html'
      ])
      .pipe(replace('../img/', imgHost))
      .pipe(inlineCss({
        preserveMediaQueries: true,
        applyWidthAttributes: true,
        applyTableAttributes: true,
        removeHtmlSelectors: true
      }))
      .pipe(gulp.dest(buildPath));
  });
});
